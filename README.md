# README #

### What is this repository for? ###

* Demos and examples for new JUnit 5 features (Version 1.0.0)

### How do I get set up? ###

* To setup the Maven project call: mvn clean install
* To run all tests you need to call in a terminal: java -jar consoleLauncher/junit-platform-console-standalone-1.0.0-M5.jar --classpath target/test-classes:target/classes --include-classname ^.*Demo?$ --scan-classpath
