package de.mike.training.junit5.beans;

public class Address {

   private String streetname;
   private String streetno;
   private String postalCode;
   private String city;

   public String getStreetname() {
      return streetname;
   }

   public void setStreetname(final String streetname) {
      this.streetname = streetname;
   }

   public String getStreetno() {
      return streetno;
   }

   public void setStreetno(final String streetno) {
      this.streetno = streetno;
   }

   public String getPostalCode() {
      return postalCode;
   }

   public void setPostalCode(final String postalCode) {
      this.postalCode = postalCode;
   }

   public String getCity() {
      return city;
   }

   public void setCity(final String city) {
      this.city = city;
   }

}
