package de.mike.training.junit5.beans;

import java.util.Date;

public class Person {

   private String firstname;

   private String lastname;

   private Date birthdate;

   public Person() {
      firstname = "Michael";
      lastname = "Albrecht";
      birthdate = new Date(41385600000L);
   }

   public String getFirstname() {
      return firstname;
   }

   public void setFirstname(final String firstname) {
      this.firstname = firstname;
   }

   public String getLastname() {
      return lastname;
   }

   public void setLastname(final String lastname) {
      this.lastname = lastname;
   }

   public Date getBirthdate() {
      return birthdate;
   }

   public void setBirthdate(final Date birthdate) {
      this.birthdate = birthdate;
   }

}
