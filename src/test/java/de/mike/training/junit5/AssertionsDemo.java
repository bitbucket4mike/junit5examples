package de.mike.training.junit5;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import de.mike.training.junit5.beans.Person;

class AssertionsDemo {

   @Test
   void standardAssertions() {
      assertEquals(2, 2);
      assertEquals(4, 4, "The optional assertion message is now the last parameter.");
      assertTrue(2 == 2, () -> "Assertion messages can be lazily evaluated -- "
            + "to avoid constructing complex messages unnecessarily.");
   }

   @Test
   void groupedAssertions() {
      // In a grouped assertion all assertions are executed, and any
      // failures will be reported together.
      Person person = new Person();
      person.setFirstname("John");
      person.setLastname("User");

      assertAll("address", () -> assertEquals("John", person.getFirstname()),
            () -> assertEquals("User", person.getLastname()));
   }

   @Test
   void exceptionTesting() {
      Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
         throw new IllegalArgumentException("a message");
      });
      assertEquals("a message", exception.getMessage());
   }

}