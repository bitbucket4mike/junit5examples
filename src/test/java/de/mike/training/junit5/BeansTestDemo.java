package de.mike.training.junit5;

import static org.hamcrest.CoreMatchers.is;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertThat;

import de.mike.training.junit5.beans.Address;
import de.mike.training.junit5.beans.Person;

@Disabled("Wrong ParameterResolver Implementation")
public class BeansTestDemo {

   @Test
   void testePersonAndAddress(final Person person, final Address address) {
      assertThat(person.getFirstname(), is("Michael"));
      assertThat(address.getStreetno(), is("66"));
   }
}
