package de.mike.training.junit5;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("Ein Testlauf für unterschiedliche Testbezeichner")
class DisplayNameDemos {

   @Test
   @DisplayName("Mein selbst definierter Name mit Leerzeichen")
   void testWithDisplayNameContainingSpaces() {
   }

   @Test
   @DisplayName("⌚ long running test ⏳ ...could take time until 🌛")
   void testWithDisplayNameContainingSpecialCharacters() {
   }

   @Test
   @DisplayName("😱")
   void testWithDisplayNameContainingEmoji() {
   }

   @Test
   @DisplayName("It's a 🚬 test.")
   public void testSmokersTest() throws Exception {

   }

}