package de.mike.training.junit5;

import java.util.logging.Logger;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import de.mike.training.junit5.extensions.AfterAllCallbackExtensionDemo;
import de.mike.training.junit5.extensions.AfterEachCallBackExtensionDemo;
import de.mike.training.junit5.extensions.AfterEachMethodAdapterExtensionDemo;
import de.mike.training.junit5.extensions.AfterTestExecutionCallbackExtensionDemo;
import de.mike.training.junit5.extensions.BeforeAllCallbackExtensionDemo;
import de.mike.training.junit5.extensions.BeforeEachCallBackExtensionDemo;
import de.mike.training.junit5.extensions.BeforeEachMethodAdapterExtensionDemo;
import de.mike.training.junit5.extensions.BeforeTestExecutionCallbackExtensionDemo;
import de.mike.training.junit5.extensions.ExecutionConditionExtensionDemo;
import de.mike.training.junit5.extensions.TimerExtension;

// formatter:off
@ExtendWith({ 
   BeforeAllCallbackExtensionDemo.class, 
   BeforeEachCallBackExtensionDemo.class,
   BeforeEachMethodAdapterExtensionDemo.class, 
   BeforeTestExecutionCallbackExtensionDemo.class, 
   AfterTestExecutionCallbackExtensionDemo.class,
   AfterEachMethodAdapterExtensionDemo.class,
   AfterEachCallBackExtensionDemo.class, 
   AfterAllCallbackExtensionDemo.class, 
   ExecutionConditionExtensionDemo.class,
   })

// formatter:on
public class ExtensionDemo {

   private static final Logger LOG = Logger.getLogger(TimerExtension.class.getName());

   @BeforeAll
   static void beforeClass() {
      LOG.info("Ich bin beforeAll.");
   }

   @BeforeEach
   void beforeEach() {
      LOG.info("Ich bin beforeEach.");
   }

   @Test
   @DisplayName("ExtensionDemo 1")
   void test1() {
      LOG.info("Ich bin der Testlauf 1.");
   }

   @Test
   @DisplayName("ExtensionDemo 2")
   void test2() {
      LOG.info("Ich bin der Testlauf 2.");
   }

   @AfterEach
   void afterEach() {
      LOG.info("Ich bin afterEach");
   }

   @AfterAll
   static void afterAll() {
      LOG.info("Ich bin afterAll");
   }
}
