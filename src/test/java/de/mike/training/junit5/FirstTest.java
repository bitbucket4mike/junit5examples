package de.mike.training.junit5;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertThat;

//@Disabled("For Demo comment this line")
class FirstTest {

   @Test
   void myFirstTest() {
      assertThat("1 + 1 should equal 2", 1 + 1, is(2));
   }

   @Test
   void myFirstTestWithSupplier() {
      assertTrue(1 + 1 == 2, () -> "1 + 1 should equal 2");

      assertAll("Check all assertions and report each", () -> assertTrue(1 + 2 == 5, "1 + 2 should equal 3"),
            () -> assertTrue(1 + 3 == 4, "1 + 3 should equal 4"), () -> assertTrue(2 + 2 == 5, "2 + 2 should equal 4"));
   }

}
