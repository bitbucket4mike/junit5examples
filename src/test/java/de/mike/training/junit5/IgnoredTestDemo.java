package de.mike.training.junit5;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

public class IgnoredTestDemo {

   @Disabled("This shows how to disable a test.")
   @Test
   public void test() {
      // nothing will be done
   }
}
