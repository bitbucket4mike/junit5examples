package de.mike.training.junit5;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertThat;

@RunWith(JUnitPlatform.class)
class JUnit5By4Demo {

   @Test
   void myFirstTest() {
      assertThat("1 + 1 should equal 2", 1 + 1, is(2));
   }

   @Test
   @Disabled("For Demo comment this line")
   void myFirstTestWithSupplier() {
      assertTrue(1 + 1 == 2, () -> "1 + 1 should equal 2");

      assertAll("Check all assertions and report each", 
            () -> assertTrue(1 + 2 == 3, "1 + 2 should equal 3"),
            () -> assertTrue(1 + 3 == 5, "1 + 3 should equal 4"), 
            () -> assertTrue(2 + 2 == 5, "2 + 2 should equal 4"));
   }

}
