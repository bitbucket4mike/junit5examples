package de.mike.training.junit5;

import static org.hamcrest.CoreMatchers.is;

import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.RepetitionInfo;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import static org.junit.Assert.assertThat;

@TestInstance(Lifecycle.PER_CLASS)
class RepetitionDemo {

   private int wert = 0;

   @RepeatedTest(value = 4, name = "Mein Test: {currentRepetition} / {totalRepetitions}")
   void testRepetition(final RepetitionInfo repetitionInfo) {
      wert++;
      assertThat(wert, is(repetitionInfo.getCurrentRepetition() * repetitionInfo.getCurrentRepetition()));
      System.out.println(wert);
   }

}
