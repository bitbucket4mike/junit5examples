package de.mike.training.junit5;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import de.mike.training.junit5.annotations.SingleTest;

@Tag("complete_test")
public class TaggingDemo {

   @Test
   @SingleTest
   public void test1() {
      System.out.println("Test 1 was running");
   }

   @Test
   public void test2() {
      System.out.println("Test 2 was running");
   }

   @Test
   @Tag("single_test")
   @Tag("other_test")
   public void test3() {
      System.out.println("Test 3 was running");
   }

}
