package de.mike.training.junit5;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

@DisplayName("TestInfo Demo")
class TestInfoDemo {

   TestInfoDemo(final TestInfo testInfo) {
      assertEquals("TestInfo Demo", testInfo.getDisplayName());
   }

   @BeforeAll
   static void initForAll() {
      ;
   }

   @BeforeEach
   void init(final TestInfo testInfo) {
      String displayName = testInfo.getDisplayName();
      assertTrue(displayName.equals("TEST 1") || displayName.equals("test2"));
   }

   @Test
   @DisplayName("TEST 1")
   @Tag("my-tag")
   void test1(final TestInfo testInfo) {
      assertEquals("TEST 1", testInfo.getDisplayName());
      assertTrue(testInfo.getTags().contains("my-tag"));
   }

   @Test
   @DisplayName("test2")
   void test2() {
   }

}