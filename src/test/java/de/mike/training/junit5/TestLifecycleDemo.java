package de.mike.training.junit5;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
public class TestLifecycleDemo {

   private int index = 0;

   @Test
   public void test1() throws Exception {
      index++;
      System.out.println("Ich bin Objekt " + index + " mit ID <" + this + ">");
   }

   @Test
   public void test2() throws Exception {
      index++;
      System.out.println("Ich bin Objekt " + index + " mit ID <" + this + ">");
   }

   @Test
   public void test3() throws Exception {
      index++;
      System.out.println("Ich bin Objekt " + index + " mit ID <" + this + ">");
   }
}
