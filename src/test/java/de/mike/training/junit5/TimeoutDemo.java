package de.mike.training.junit5;

import static java.time.Duration.ofMillis;
import static java.time.Duration.ofMinutes;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTimeout;
import static org.junit.jupiter.api.Assertions.assertTimeoutPreemptively;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

@Disabled("For Demo comment this line")
class TimeoutDemo {

   @Test
   void timeoutNotExceeded() {
      // The following assertion succeeds.
      assertTimeout(ofMinutes(2), () -> {
         // Perform task that takes less than 2 minutes.
      });
   }

   @Test
   void timeoutNotExceededWithResult() {
      // The following assertion succeeds, and returns the supplied object.
      String actualResult = assertTimeout(ofMinutes(2), () -> {
         return "a result";
      });
      assertEquals("a result", actualResult);
   }

   @Test
   void timeoutNotExceededWithMethod() {
      // The following assertion invokes a method reference and returns an
      // object.
      String actualGreeting = assertTimeout(ofMinutes(2), TimeoutDemo::greeting);
      assertEquals("hello world!", actualGreeting);
   }

   @Test
   void timeoutExceeded() {
      // The following assertion fails with an error message similar to:
      // execution exceeded timeout of 10 ms by 91 ms
      assertTimeout(ofMillis(10), () -> {
         // Simulate task that takes more than 10 ms.
         Thread.sleep(100);
      });
   }

   @Test
   void timeoutExceededWithPreemptiveTermination() {
      // The following assertion fails with an error message similar to:
      // execution timed out after 10 ms
      assertTimeoutPreemptively(ofMillis(10), () -> {
         // Simulate task that takes more than 10 ms.
         Thread.sleep(100);
      });
   }

   private static String greeting() {
      return "hello world!";
   }

}