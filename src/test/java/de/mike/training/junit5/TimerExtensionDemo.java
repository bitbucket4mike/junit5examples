package de.mike.training.junit5;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import de.mike.training.junit5.extensions.TimerExtension;

@ExtendWith(TimerExtension.class)
class TimerExtensionDemo {

   @Test
   void sleep20ms() throws Exception {
      Thread.sleep(20);
   }

   @Test
   void sleep50ms() throws Exception {
      Thread.sleep(50);
   }
}
