package de.mike.training.junit5.arabic2roman;

import static org.hamcrest.CoreMatchers.is;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.Assert.assertThat;

public class Arabic2RomanConverterTest {

   @ParameterizedTest(name = "{0} konvertiert in \"{1}\"")
   @CsvSource({ "0, ''", "1, I", "2, II", "4, IV", "5, V", "9, IX", "40, XL", "90, XC", "400, CD", "500, D", "900, CM",
         "1000, M", "2330, MMCCCXXX", "1984, MCMLXXXIV" })
   @DisplayName("Konvertiere arabische Zahl in römische Zeichen")
   void testDifferentValues(final int arabicNumber, final String romanChars) throws Exception {
      assertThat(new Arabic2RomanConverter().convert(arabicNumber), is(romanChars));
   }
}
