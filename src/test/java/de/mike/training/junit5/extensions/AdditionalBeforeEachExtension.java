package de.mike.training.junit5.extensions;

import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

public class AdditionalBeforeEachExtension implements BeforeEachCallback {

   @Override
   public void beforeEach(final ExtensionContext arg0) throws Exception {
      System.out
            .println("Ich laufe auch vor dem Test:" + arg0.getDisplayName());
   }

}
