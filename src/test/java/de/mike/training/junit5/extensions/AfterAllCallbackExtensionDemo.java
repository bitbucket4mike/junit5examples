package de.mike.training.junit5.extensions;

import java.util.logging.Logger;

import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

public class AfterAllCallbackExtensionDemo implements AfterAllCallback {

   private static final Logger LOG = Logger.getLogger(AfterAllCallbackExtensionDemo.class.getName());

   @Override
   public void afterAll(final ExtensionContext context) throws Exception {
      LOG.info("AfterAllCallback:" + context.getDisplayName());
   }

}
