package de.mike.training.junit5.extensions;

import java.util.logging.Logger;

import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

public class AfterEachCallBackExtensionDemo implements AfterEachCallback {

   private static final Logger LOG = Logger.getLogger(AfterEachCallBackExtensionDemo.class.getName());

   @Override
   public void afterEach(final ExtensionContext context) throws Exception {
      LOG.info("AfterEachCallBack:" + context.getDisplayName());
   }

}
