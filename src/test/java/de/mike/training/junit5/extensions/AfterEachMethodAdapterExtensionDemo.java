package de.mike.training.junit5.extensions;

import java.util.logging.Logger;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.engine.execution.AfterEachMethodAdapter;
import org.junit.jupiter.engine.extension.ExtensionRegistry;

public class AfterEachMethodAdapterExtensionDemo implements AfterEachMethodAdapter {

   private static final Logger LOG = Logger.getLogger(AfterEachMethodAdapterExtensionDemo.class.getName());

   @Override
   public void invokeAfterEachMethod(final ExtensionContext context, final ExtensionRegistry registry)
         throws Throwable {
      LOG.info("AfterEachMethodAdapter:" + context.getDisplayName());
   }

}
