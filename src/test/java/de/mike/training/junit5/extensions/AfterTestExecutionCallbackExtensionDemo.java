package de.mike.training.junit5.extensions;

import java.util.logging.Logger;

import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

public class AfterTestExecutionCallbackExtensionDemo implements AfterTestExecutionCallback {

   private static final Logger LOG = Logger.getLogger(AfterTestExecutionCallbackExtensionDemo.class.getName());

   @Override
   public void afterTestExecution(final ExtensionContext context) throws Exception {
      LOG.info("AfterTestExecutionCallback:" + context.getDisplayName());
   }

}
