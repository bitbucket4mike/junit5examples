package de.mike.training.junit5.extensions;

import java.util.logging.Logger;

import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

public class BeforeAllCallbackExtensionDemo implements BeforeAllCallback {

   private static final Logger LOG = Logger.getLogger(BeforeAllCallbackExtensionDemo.class.getName());

   @Override
   public void beforeAll(final ExtensionContext context) throws Exception {
      LOG.info("BeforeAllCallback:" + context.getDisplayName());
   }

}
