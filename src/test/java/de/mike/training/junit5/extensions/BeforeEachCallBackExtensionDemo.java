package de.mike.training.junit5.extensions;

import java.util.logging.Logger;

import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

public class BeforeEachCallBackExtensionDemo implements BeforeEachCallback {

   private static final Logger LOG = Logger.getLogger(TimerExtension.class.getName());

   @Override
   public void beforeEach(final ExtensionContext arg0) throws Exception {
      LOG.info("BeforeEachCallBack:" + arg0.getDisplayName());
   }

}
