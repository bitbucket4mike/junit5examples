package de.mike.training.junit5.extensions;

import java.util.logging.Logger;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.engine.execution.BeforeEachMethodAdapter;
import org.junit.jupiter.engine.extension.ExtensionRegistry;

public class BeforeEachMethodAdapterExtensionDemo implements BeforeEachMethodAdapter {

   private static final Logger LOG = Logger.getLogger(BeforeEachMethodAdapterExtensionDemo.class.getName());

   @Override
   public void invokeBeforeEachMethod(final ExtensionContext context, final ExtensionRegistry registry)
         throws Throwable {
      LOG.info("BeforeEachMethodAdapter:" + context.getDisplayName());
   }

}
