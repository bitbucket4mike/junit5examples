package de.mike.training.junit5.extensions;

import java.util.logging.Logger;

import org.junit.jupiter.api.extension.BeforeTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

public class BeforeTestExecutionCallbackExtensionDemo implements BeforeTestExecutionCallback {

   private static final Logger LOG = Logger.getLogger(BeforeTestExecutionCallbackExtensionDemo.class.getName());

   @Override
   public void beforeTestExecution(final ExtensionContext context) throws Exception {
      LOG.info("BeforeTestExecutionCallback:" + context.getDisplayName());
   }

}
