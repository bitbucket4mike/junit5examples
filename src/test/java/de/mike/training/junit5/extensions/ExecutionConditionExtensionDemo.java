package de.mike.training.junit5.extensions;

import java.util.logging.Logger;

import org.junit.jupiter.api.extension.ConditionEvaluationResult;
import org.junit.jupiter.api.extension.ExecutionCondition;
import org.junit.jupiter.api.extension.ExtensionContext;

public class ExecutionConditionExtensionDemo implements ExecutionCondition {

   private static final Logger LOG = Logger.getLogger(ExecutionConditionExtensionDemo.class.getName());

   @Override
   public ConditionEvaluationResult evaluateExecutionCondition(final ExtensionContext context) {
      LOG.info("ExecutionCondition:" + context.getDisplayName());
      if (context.getDisplayName().contains("ExtensionDemo")) {
         return ConditionEvaluationResult.enabled("Bedingung erfüllt.");
      }
      return ConditionEvaluationResult.disabled("Bedingung NICHT erfüllt.");
   }

}
