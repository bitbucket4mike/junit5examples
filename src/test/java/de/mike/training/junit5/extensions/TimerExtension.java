package de.mike.training.junit5.extensions;

import java.lang.reflect.Method;
import java.util.logging.Logger;

import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.BeforeTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ExtensionContext.Namespace;
import org.junit.jupiter.api.extension.ExtensionContext.Store;

public class TimerExtension implements BeforeTestExecutionCallback, AfterTestExecutionCallback {

   private static final Logger LOG = Logger.getLogger(TimerExtension.class.getName());

   @Override
   public void afterTestExecution(final ExtensionContext context) throws Exception {
      Method testMethod = context.getRequiredTestMethod();
      long start = getStore(context).remove(testMethod, long.class);
      long duration = System.currentTimeMillis() - start;

      LOG.info(() -> String.format("Method [%s] took %s ms.", testMethod.getName(), duration));
   }

   @Override
   public void beforeTestExecution(final ExtensionContext context) throws Exception {
      getStore(context).put(context.getRequiredTestMethod(), System.currentTimeMillis());
   }

   private Store getStore(final ExtensionContext context) {
      return context.getStore(Namespace.create(getClass(), context));
   }

}
