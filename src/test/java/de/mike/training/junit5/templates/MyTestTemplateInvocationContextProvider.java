package de.mike.training.junit5.templates;

import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.extension.Extension;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolver;
import org.junit.jupiter.api.extension.TestTemplateInvocationContext;
import org.junit.jupiter.api.extension.TestTemplateInvocationContextProvider;

public class MyTestTemplateInvocationContextProvider
      implements TestTemplateInvocationContextProvider {

   @Override
   public Stream<TestTemplateInvocationContext> provideTestTemplateInvocationContexts(
         final ExtensionContext arg0) {
      return Stream.of(invocationContext("foo"), invocationContext("bar"));
   }

   @Override
   public boolean supportsTestTemplate(final ExtensionContext arg0) {
      return true;
   }

   private TestTemplateInvocationContext invocationContext(
         final String parameter) {
      return new TestTemplateInvocationContext() {
         @Override
         public String getDisplayName(final int invocationIndex) {
            return parameter + " Aufruf Nr.:" + invocationIndex;
         }

         @Override
         public List<Extension> getAdditionalExtensions() {
            return Collections.singletonList(new ParameterResolver() {

               @Override
               public boolean supportsParameter(
                     final ParameterContext parameterContext,
                     final ExtensionContext extensionContext) {
                  return parameterContext.getParameter().getType()
                        .equals(String.class);
               }

               @Override
               public Object resolveParameter(
                     final ParameterContext parameterContext,
                     final ExtensionContext extensionContext) {
                  return parameter;
               }

            });
         }
      };
   }
}
