package de.mike.training.junit5.templates;

import static org.hamcrest.CoreMatchers.is;

import org.junit.jupiter.api.TestTemplate;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.junit.Assert.assertThat;

public class TestTemplateDemo {

   @TestTemplate
   @ExtendWith(MyTestTemplateInvocationContextProvider.class)
   void testTemplate(final String parameter) {
      assertThat(parameter.length(), is(3));
   }
}
